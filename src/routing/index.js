import Messaging from 'components/messaging'
import Profile from 'components/profile'

export const configuration = {
  redirect: '/messaging'
}

export const routes = [
  {
    name: 'messaging',
    title: 'messaging',
    path: '/messaging',
    component: Messaging
  },
  {
    name: 'profile',
    title: 'profile',
    path: '/profile',
    component: Profile
  }
]
