import { combineEpics, ofType } from 'redux-observable'
import { delay, mapTo, mergeMap } from 'rxjs/operators'
import 'rxjs/add/operator/map'

import { MESSAGEIN_MESSAGE_SUBMIT, MESSAGING_SEND_MESSAGE } from './constants'
import { sendMessage, messageRecieved } from './actions'

export const messageSubmitEpic = action =>
  action.pipe(ofType(MESSAGEIN_MESSAGE_SUBMIT), mapTo(sendMessage()))

export const sendMessageAndRecieveEpic = action =>
  action.pipe(
    ofType(MESSAGING_SEND_MESSAGE),
    delay(1000),
    mergeMap(item => messageRecieved(item)) // add new item and remove load
  )

export default combineEpics(messageSubmitEpic, sendMessageAndRecieveEpic)
