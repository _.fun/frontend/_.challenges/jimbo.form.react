import {
  MESSAGING_MESSAGE_SUBMIT,
  MESSAGING_SEND_MESSAGE,
  MESSAGING_MESSAGE_RECIEVED
} from './constants'

export const messageSubmit = value => ({
  type: MESSAGING_MESSAGE_SUBMIT,
  value
})

export const sendMessage = value => ({
  type: MESSAGING_SEND_MESSAGE,
  value
})

export const messageRecieved = value => ({
  type: MESSAGING_MESSAGE_RECIEVED,
  value
})

export const sendMessageAction = dispatch => value =>
  dispatch(sendMessage(value))

export const messageRecievedAction = dispatch => value =>
  dispatch(messageRecieved(value))
