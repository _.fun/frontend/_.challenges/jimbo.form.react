import React from 'react'

import { Block } from 'components/common/loading'
import { Grid, Name, Email, Message } from './containers'

export default () => (
  <Grid>
    <Name>
      <Block />
    </Name>
    <Email>
      <Block />
    </Email>
    <Message>
      <Block />
    </Message>
  </Grid>
)
