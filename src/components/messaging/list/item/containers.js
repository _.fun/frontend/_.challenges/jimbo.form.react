import styled, { css } from 'styled-components'
import { darken } from 'polished'

export const Grid = styled.div`
  display: grid;
  padding: 0.5em;

  grid-gap: 1rem;

  grid-template:
    'name'
    'email'
    'message';

  ${({ theme: { background } }) => css`
    background-color: ${darken(0.05, background)};
  `};
`

export const Name = styled.div`
  grid-area: name;
`

export const Email = styled.div`
  grid-area: email;
`

export const Message = styled.div`
  grid-area: message;
`
