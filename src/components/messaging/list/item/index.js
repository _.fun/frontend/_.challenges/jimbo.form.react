import React from 'react'
import PropTypes from 'prop-types'

import { Grid, Name, Email, Message } from './containers'

const Item = ({ name, email, message }) => (
  <Grid>
    <Name>
      <span>Name: {name}</span>
    </Name>
    <Email>
      <span>Email: {email}</span>
    </Email>
    <Message>
      <span>Phone: {message}</span>
    </Message>
  </Grid>
)

export const ItemPropType = {
  name: PropTypes.string.isRequired,
  email: PropTypes.string.isRequired,
  message: PropTypes.string.isRequired
}

Item.propTypes = ItemPropType

export default Item
