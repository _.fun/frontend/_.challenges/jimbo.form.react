import styled from 'styled-components'
import { mediaMin } from 'styled'

export const Grid = styled.div`
  grid-area: form;

  display: grid;
  grid-auto-flow: column;

  grid-template:
    'name'
    'email'
    'message'
    'action';
`

export const Name = styled.div`
  grid-area: name;

  display: flex;
  flex-flow: column;

  ${mediaMin.medium`
    flex-flow: row;
  `};
`

export const Email = styled.div`
  grid-area: email;

  display: flex;
  flex-flow: column;

  ${mediaMin.medium`
    flex-flow: row;
  `};
`

export const Message = styled.div`
  grid-area: message;

  display: flex;
  flex-flow: column;

  ${mediaMin.medium`
    flex-flow: row;
  `};
`

export const Action = styled.div`
  grid-area: action;
`
