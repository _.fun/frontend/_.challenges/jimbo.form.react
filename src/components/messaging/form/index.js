import React from 'react'
import PropTypes from 'prop-types'
//import { FaPlus } from 'react-icons/lib/fa'

import { Button } from 'components/common'

import Loading from './loading'
import { Grid, Name, Email, Message, Action } from './containers'

const Form = ({ loading, name, email, message, sendMessageAction }) => {
  if (loading) return <Loading name={name} email={email} message={message} />

  return (
    <Grid>
      <Name>
        <div>
          <label>Name:</label>
        </div>
        <div>input: {name}</div>
      </Name>

      <Email>
        <div>
          <label>Email:</label>
        </div>
        <div>input: {email}</div>
      </Email>

      <Message>
        <div>
          <label>Message:</label>
        </div>
        <div>input: {message}</div>
      </Message>

      <Action>
        <Button isCircular isAccent onClick={sendMessageAction}>
          Send
        </Button>
      </Action>
    </Grid>
  )
}

export const FormPropTypes = {
  loading: PropTypes.bool,
  name: PropTypes.string.isRequired,
  email: PropTypes.string.isRequired,
  message: PropTypes.string.isRequired,
  sendMessageAction: PropTypes.func.isRequired
}

Form.propTypes = FormPropTypes

export default Form
