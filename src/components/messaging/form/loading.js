import React from 'react'

import { Button } from 'components/common'

import { Grid, Name, Email, Message, Action } from './containers'

export default ({ name, email, message }) => (
  <Grid>
    <Name>
      <div>
        <label>Name:</label>
      </div>
      <div>input: {name}</div>
    </Name>

    <Email>
      <div>
        <label>Email:</label>
      </div>
      <div>input: {email}</div>
    </Email>

    <Message>
      <div>
        <label>Message:</label>
      </div>
      <div>input: {message}</div>
    </Message>

    <Action>
      <Button isCircular isAccent disabled>
        Send
      </Button>
    </Action>
  </Grid>
)
