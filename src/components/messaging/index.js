import React from 'react'
import { connect } from 'react-redux'

import { Grid } from './containers'
import Form, { FormPropTypes } from './form'
import List, { ListPropTypes } from './list'

import { sendMessageAction } from './actions'

const Messaging = ({
  name,
  email,
  message,
  list,
  loading,
  sendMessageAction
}) => (
  <Grid>
    <Form
      loading={loading}
      name={name}
      email={email}
      message={message}
      sendMessageAction={sendMessageAction}
    />
    <List list={list} loading={loading} />
  </Grid>
)

Messaging.propTypes = {
  ...ListPropTypes,
  ...FormPropTypes
}

const mapStateToProps = ({
  messaging: { name, email, message, list, loading }
}) => ({
  name,
  email,
  message,
  list,
  loading
})
const mapDispatchToProps = dispatch => ({
  sendMessageAction: sendMessageAction(dispatch)
})

export default connect(mapStateToProps, mapDispatchToProps)(Messaging)
