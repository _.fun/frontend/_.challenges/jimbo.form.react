import { MESSAGING_SEND_MESSAGE, MESSAGING_MESSAGE_RECIEVED } from './constants'

// for simplicity of state management
const clearFormState = {
  name: '',
  email: '',
  message: ''
}

export const initialState = {
  ...clearFormState,
  list: [],
  loading: false
}

export default (state = initialState, { type, value }) => {
  switch (type) {
    case MESSAGING_SEND_MESSAGE:
      return {
        ...state,
        loading: false
      }
    case MESSAGING_MESSAGE_RECIEVED:
      return {
        ...state,
        // didn't want to overcomplicate form
        ...clearFormState,
        list: [...state.list, value],
        loading: false
      }
    default:
      return state
  }
}
