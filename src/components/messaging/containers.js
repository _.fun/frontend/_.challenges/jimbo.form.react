import styled from 'styled-components'

export const Grid = styled.div`
  display: grid;

  grid-template:
    'form'
    'list';
  grid-gap: 1rem;

  align-items: center;
`
