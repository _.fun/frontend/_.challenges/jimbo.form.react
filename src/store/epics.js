import { combineEpics } from 'redux-observable'

import messaging from 'components/messaging/epic'

export default combineEpics(messaging)
